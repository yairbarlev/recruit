<?php

namespace App\Http\Controllers;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Http\Request;

class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all(); // :: = db static function same as * at sql
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index',compact('candidates','users','statuses')); //
    }
    public function changeUser($cid , $uid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        return redirect('candidates');
    }

    public function changeStatus($cid , $sid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->status_id = $sid;
        $candidate->save();
        return redirect('candidates');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate();
        $candidate->create($request->all());
        return redirect('candidates');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        return view('candidates.edit', compact('candidate'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $candidate = candidate::findOrFail($id);
        $candidate->update($request->all());
        return redirect('candidates');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');
        //
    }
}
