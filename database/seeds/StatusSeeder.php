<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StatusSeeder extends Seeder
{
    /**before interview, not fit, sent to manager, not fit professionally, accepted to work

     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'id' => 1,
            'name' => 'before interview'
        ]);
        DB::table('statuses')->insert([
            'id' => 2,
            'name' => 'not fit'
        ]);
        DB::table('statuses')->insert([
            'id' => 3,
            'name' => 'sent to manager'
        ]);
        DB::table('statuses')->insert([
            'id' => 4,
            'name' => 'not fit professionally'
        ]);
        DB::table('statuses')->insert([
            'id' => 5,
            'name' => 'accepted to work'
        ]);

    }
}
