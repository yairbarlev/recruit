@extends('layouts.app')

@section('title','List of candidates')

@section('content')
    <div><a href = "{{url('/candidates/create')}}">Add new candidate</a></div>
    <h1>List of candidates </h1>
        <table class = "table table-dark">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Status</th><th>Owner</th><th>Created</th><th>Updated</th>
            </tr>
            <!-- the table data -->
            @foreach($candidates as $Candidate)
            <tr>
                <td>{{$Candidate ->id}}   </td> <!--show value -->
                <td>{{$Candidate ->name}}   </td>
                <td>{{$Candidate ->email}}   </td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($Candidate->status_id))
                            {{$Candidate->status->name}}
                        @else
                        Assign status
                        @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($statuses as $status)
                          <a class="dropdown-item" href="{{route('candidate.changestatus',[$Candidate ->id,$status->id])}}">{{$status->name}}</a>
                          @endforeach

                          <a class="dropdown-item" href="{{route('candidate.changeuser',[$Candidate ->id])}}">No status</a>
                        </div>
                      </div>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($Candidate->user_id))
                            {{$Candidate->owner->name}}
                        @else
                        Assign owner
                        @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user)
                          <a class="dropdown-item" href="{{route('candidate.changeuser',[$Candidate ->id,$user->id])}}">{{$user->name}}</a>
                          @endforeach

                          <a class="dropdown-item" href="{{route('candidate.changeuser',[$Candidate ->id])}}">No user</a>
                        </div>
                      </div>
                </td>
                <td>{{$Candidate ->created_at}}   </td>
                <td>{{$Candidate ->updated_at}}   </td>

                <td><a href = "{{route('candidates.edit', $Candidate ->id)}}">Edit candidate</a></td>
                <td><a href = "{{route('candidate.delete', $Candidate ->id)}}">Delete candidate</a></td>
            </tr>
            @endforeach

        <table>
 @endsection
